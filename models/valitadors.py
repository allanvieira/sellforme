__author__ = 'allan'
# -*- coding: utf-8 -*-

##Validadores de loja B2W
B2WLoja.idloja.requires = [IS_NOT_EMPTY(),IS_NOT_IN_DB(db, 'b2w_loja.idloja')]
B2WLoja.token.requires = [IS_NOT_EMPTY(),IS_NOT_IN_DB(db, 'b2w_loja.token')]
B2WLoja.descricao.requires = IS_NOT_EMPTY()
B2WLoja.owner.requires = IS_IN_DB(db, 'auth_user.id', '%(first_name)s')

##Validadores de sku B2w
B2WSku.idloja.requires = IS_IN_DB(db, 'b2w_loja.id', '%(descricao)s')
B2WSku.owner.requires = IS_IN_DB(db, 'auth_user.id', '%(first_name)s')
B2WSku.idsku.requires = IS_NOT_EMPTY()
B2WSku.loadedat.requires = [IS_NOT_EMPTY(),IS_DATETIME(format='%d-%m-%Y %H:%M:%S')]
B2WSku.updatedat.requires = [IS_NOT_EMPTY(),IS_DATETIME(format='%d-%m-%Y %H:%M:%S')]