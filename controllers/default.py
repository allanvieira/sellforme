# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
#########################################################################
import base64
import urllib
import urllib2
import json
import datetime
import httplib
import sys
reload(sys)
sys.setdefaultencoding("utf-8")

class BtwoW(object):
    """Build a B2W object"""

    def __init__(self,idloja,idskus):
        rowLoja = db(db.b2w_loja.id==idloja).select()
        self._loja= idloja
        self._skus = idskus
        self._token = 'Basic ' + base64.b64encode(rowLoja[0].token+':')
        print self._token

    #Metodo que seta status invalido do sku
    def setInvalid(self,sku,error):
        #captura data e hora de inclusao
        now = datetime.datetime.now()
        ##procure pelo sku que ja foi incluido sem informacao nenhuma e realiza os update
        query=(db.b2w_sku.idsku==sku)&(db.b2w_sku.idloja==self.loja)
        db(query).update(
            situation = error.replace('\'',''),
            updatedat = now.strftime("%d-%m-%Y %H:%M:%S")
        )

    #Metodo que monta a url de busca de precos e concorrentes na americanas
    def montaUrlPreco(self,name,idb2w):
        nameEncode = name.lower()
        nameEncode.replace(' ','-')
        acentos = {'á':'a','ã':'ã','à':'a','ä':'a',
                   'é':'e','ẽ':'e','è':'e','ë':'e',
                   'í':'i','ĩ':'i','ò':'o','ö':'ö',
                   'ú':'u','ũ':'u','ù':'u','ü':'u'}
        for i in acentos:
            print i
            print acentos[i]
            nameEncode.replace(i,acentos[i])
        urlpreco = 'http://www.americanas.com.br/parceiros/'+idb2w+'/'+nameEncode+'?codItemFusion='+idb2w
        print urlpreco
        return urlpreco

    #Metodo que realiza o update de um sku atraves de seu json retornado pela api
    def updateInfo(self,sku,skuJson):
        ##Valida campos não obrigatorios e tambem tranforma dados aninhados em colunas
        enable = ''
        urlImage = []
        idB2W = ''
        description = ''
        length = 0
        heigth = 0
        width = 0
        weight = 0
        subCategoryId = 0
        subFamilyId = 0
        categoryId = 0
        familyId = 0
        urlPrice = ''
        ean = []
        if 'enable' in skuJson.keys():
            enable = skuJson['enable']
        if 'urlImage' in skuJson.keys():
            for url in skuJson['urlImage']:
                urlImage.append(url)
        if 'idB2W' in skuJson.keys():
            idB2W = skuJson['idB2W']
            urlPrice = self.montaUrlPreco(skuJson['name'],skuJson['idB2W'])
        if 'description' in skuJson.keys():
            description = skuJson['description']
        if 'length' in skuJson.keys():
            length = skuJson['length']
        if 'heigth' in skuJson.keys():
            heigth = skuJson['heigth']
        if 'width' in skuJson.keys():
            width = skuJson['width']
        if 'weight' in skuJson.keys():
            weight = skuJson['weight']
        if 'marketStructure' in skuJson.keys():
            subCategoryId = skuJson['marketStructure']['subCategoryId']
            subFamilyId = skuJson['marketStructure']['subFamilyId']
            categoryId = skuJson['marketStructure']['categoryId']
            familyId = skuJson['marketStructure']['familyId']
        if 'ean' in skuJson.keys():
            for e in skuJson['ean']:
                ean.append(e)
        #captura data e hora de inclusao
        now = datetime.datetime.now()
        ##procure pelo sku que ja foi incluido sem informacao nenhuma e realiza os update
        query=(db.b2w_sku.idsku==sku)&(db.b2w_sku.idloja==self.loja)
        db(query).update(
            name = skuJson['name'],
            updatedat = now.strftime("%Y-%m-%d %H:%M:%S"),
            urlprice = urlPrice,
            linksubmarino = skuJson['B2WLinks']['submarino'],
            linkamericanas = skuJson['B2WLinks']['americanas'],
            linkshoptime = skuJson['B2WLinks']['shoptime'],
            description = description.replace('\'',''),
            ean = ean,
            height = heigth,
            width = width,
            length = length,
            weight = weight,
            stockquantity = skuJson['stockQuantity'],
            enable = enable,
            price = skuJson['price']['sellPrice'],
            pricesubmarino = skuJson['brandPrices'][0]['sellPrice'],
            priceamericanas = skuJson['brandPrices'][1]['sellPrice'],
            priceshoptime = skuJson['brandPrices'][2]['sellPrice'],
            urlimages = urlImage,
            categoryid = categoryId,
            subcategoryid = subCategoryId,
            familyid = familyId,
            subfamilyid = subFamilyId,
            crossdocking = skuJson['crossDocking'],
            situation = skuJson['situation'].replace('\'',''),
            idb2w = idB2W
        )

    ##Metodo que faz a requisicao do JSON do sku para a API
    def refreshInfo(self):
        URL_PRODUCAO = 'https://api-marketplace.bonmarketplace.com.br'
        URL_SANDBOX = 'https://api-sandbox.bonmarketplace.com.br'
        URL_APP = URL_PRODUCAO
        SDK_VERSION = 'b2wclass001'

        headers = {
            'Authorization': self._token,
            'Accept': 'application/json',
            'User-Agent': SDK_VERSION,
            'Content-type':'application/json',
            'Charset' : 'UTF-8'
        }

        ##Realiza o loop nos skus do objeto
        for sku in self.skus:
            urlRequest = URL_APP+'/sku/'+sku
            request = urllib2.Request(urlRequest , headers=headers)
            try:
                skuResponse = urllib2.urlopen(request)
                skuJson = json.load(skuResponse)
            except urllib2.HTTPError, e:
                self.setInvalid(sku,str(e.code))
                session.flash = 'HTTPError = %s' % str(e.code)
            except urllib2.URLError, e:
                self.setInvalid(sku,str(e.reason))
                session.flash = 'URLError = %s' % str(e.reason)
            except httplib.HTTPException, e:
                self.setInvalid(sku,'HTTPException ! ')
                session.flash = 'HTTPException ! '
            except Exception:
                import traceback
                self.setInvalid(sku,traceback.format_exc())
                session.flash = 'generic exception = %s' % traceback.format_exc()
            self.updateInfo(sku,skuJson)

    @property
    def loja(self):
        return self._loja

    @property
    def skus(self):
        return self._skus

    @property
    def token(self):
        return self._token


def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    response.flash = T("Hello World")
    return dict(message=T('Welcome to web2py!'))


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


def b2w_loja():
    response.view = 'b2w/loja.html'
    grid = SQLFORM.grid(B2WLoja)
    return dict(grid=grid)


def b2w_sku():
    grid = SQLFORM.grid(B2WSku,csv=False,details=False,fields =[B2WSku.idloja, B2WSku.idsku, B2WSku.name, B2WSku.situation, B2WSku.price, B2WSku.stockquantity, B2WSku.monitorapreco, B2WSku.monitoraestoque, B2WSku.precomin, B2WSku.precomax, B2WSku.updatedat ])
    response.view = 'b2w/sku.html'

    formnew=None
    formedit=None
    skuedit=0

    #se estiver adicionando um sku
    if  len(request.args)>1 and request.args[-2]=='new' and grid.create_form:
        response.view = 'b2w/sku_new.html'
        formnew = SQLFORM.factory(B2WSku)
        if formnew.process().accepted:
            now = datetime.datetime.now()
            B2WSku.insert(idloja=formnew.vars.idloja,idsku=formnew.vars.idsku,loadedat=now.strftime("%Y-%m-%d %H:%M:%S"))
            response.flash = 'Comunicando com a API!'
            ##cria um objeto b2w para obter as informacoes da API
            bwObj = BtwoW(formnew.vars.idloja,[formnew.vars.idsku])
            bwObj.refreshInfo()
            session.flash = 'Sku incluido com sucesso: %s' % [formnew.vars.idsku]
            redirect(URL('b2w_sku'))
        elif formnew.errors:
            response.flash = 'Erros no formulário'
        else:
            if not response.flash:
                response.flash = 'Preencha o formulário'
    elif len(request.args)>1 and request.args[-3]=='edit':
    #se estive editando um sku
        skuedit = request.args[-1]
        response.view = 'b2w/sku_edit.html'
        formedit = SQLFORM(B2WSku,skuedit)

    return dict(grid=grid,formnew=formnew,formedit=formedit,skuedit=skuedit)


def b2w_sku_refresh():
    sku= request.args[-1]
    skudata = B2WSku(sku)
    bwObj = BtwoW(skudata.idloja,[skudata.idsku])
    bwObj.refreshInfo()
    session.flash = 'Sku atualizado com sucesso: %s' % [skudata.idsku]
    return response.json({'status':'ok','sku':sku})

